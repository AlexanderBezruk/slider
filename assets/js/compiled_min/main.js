
jQuery(document).ready(function($) {
	$('.multiple-items').slick({
		infinite: true,
		arrows: true,
		dots: true,
		slidesToShow: 3,
		slidesToScroll: 1
	});
	console.log($('.event-calendar'));
	$('.event-calendar').equinox();
});
